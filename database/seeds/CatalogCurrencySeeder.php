<?php

use Illuminate\Database\Seeder;

class CatalogCurrencySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'id' => 1,
                'name' => 'Гривна',
                'title' => 'грн.',
                'short_name' => 'uah',
                'rate' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Доллар',
                'title' => '$',
                'short_name' => 'usd',
                'rate' => 26.9,
            ],
            [
                'id' => 3,
                'name' => 'Евро',
                'title' => '€',
                'short_name' => 'eur',
                'rate' => 29.05,
            ],
        ];

        foreach ($currencies as $currency) {
            \App\Models\CatalogCurrency::updateOrCreate($currency, ['id' => $currency['id']]);
        }
    }
}
