<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Http\Request;

class ForceJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->segment(1) == 'api') {
            $request->headers->set('Accept', 'application/json, text/plain, */*');
        }
        return $next($request);
    }
}
