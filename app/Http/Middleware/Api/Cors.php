<?php

namespace App\Http\Middleware\Api;

use Closure;
use Illuminate\Http\Request;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if ($request->segment(1) == 'api') {
            $origin = $request->headers->get('Origin');
            $response->header('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, DELETE', 'OPTIONS', 'PATCH');
            $response->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
            $response->header('Access-Control-Allow-Origin', "$origin");
            $response->header('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }
}
