<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CatalogRequest;
use App\Services\CatalogCrudService;
use App\Services\CatalogService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CatalogController extends Controller
{

    /**
     * @var CatalogCrudService
     */
    private $catalogCrudService;

    /**
     * @var CatalogService
     */
    private $catalogService;

    public function __construct(CatalogCrudService $catalogCrudService, CatalogService $catalogService)
    {
        $this->catalogCrudService = $catalogCrudService;
        $this->catalogService = $catalogService;
    }

    public function index(CatalogRequest $request, $id_category)
    {
        $validated = $request->validated();

        $options['id_category'] = $id_category;
        $options['currency'] = true;

        $collections = $this->catalogCrudService->get($options);
        $catalogs = $this->catalogService->filter($collections, $validated);

        return new JsonResponse([
            'products' => $catalogs,
            'totalNumberOfFilteredItems' => count($catalogs),
            'totalQuantityOfGoods' => $collections->count(),
        ]);
    }
}
