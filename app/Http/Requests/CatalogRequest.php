<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CatalogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'brand' => [
                'nullable',
                'integer'
            ],
            'availability' => [
                'nullable',
                'integer'
            ],
            'price' => [
                'array',
                'required',

            ],
            'price.from' => [
                'required',
                'integer'
            ],
            'price.to' => [
                'required',
                'integer'
            ],
            'sorting' => [
                'array',
                'required',
            ],
            'sorting.sortBy' => [
                'required',
            ],
            'sorting.isDesc' => [
                'required',
                'boolean',
            ],
        ];
    }
}
