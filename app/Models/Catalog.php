<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = ['name', 'id_brand', 'id_category', 'id_availability', 'price', 'id_currency'];

    protected $casts = [
        'id_brand' => 'integer',
        'id_category' => 'integer',
        'id_availability' => 'integer',
        'price' => 'float',
        'id_currency' => 'integer',
    ];

    public function currency()
    {
        return $this->belongsTo(CatalogCurrency::class, 'id_currency', 'id');
    }
}
