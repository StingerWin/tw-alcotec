<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CatalogCurrency extends Model
{
    protected $fillable = ['name', 'title', 'short_name', 'rate'];

    protected $casts = [
        'rate' => 'float'
    ];
}
