<?php

namespace App\Crud;

interface CrudService
{
    public function get($options = []);

}
