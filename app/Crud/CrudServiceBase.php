<?php

namespace App\Crud;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;

abstract class CrudServiceBase implements CrudService
{
    /**
     * @var string
     */
    protected $model;

    /**
     * @param array $options
     * @return LengthAwarePaginator|Collection|Model[]
     */
    public function get($options = [])
    {
        return $this->setOptions($this->model::query(), $options)->get();
    }

    /**
     * @param $q
     * @param $options
     * @return mixed
     */
    public function setOptions($q, &$options)
    {
        return $q->where($options);
    }
}
