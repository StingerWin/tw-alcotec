<?php

namespace App\Services;

use App\Crud\CrudServiceBase;
use App\Models\Catalog;
use Illuminate\Support\Arr;
use Mockery\Exception;

class CatalogCrudService extends CrudServiceBase
{
    /**
     * @var string
     */
    protected $model = Catalog::class;

    public function setOptions($q, &$options)
    {
        if (Arr::pull($options, 'currency')) {
            $q->with('currency');
        }

        return parent::setOptions($q, $options);
    }
}
