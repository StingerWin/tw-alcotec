<?php

namespace App\Services;

use App\Crud\CrudServiceBase;
use App\Models\Catalog;
use Illuminate\Support\Arr;
use Mockery\Exception;

class CatalogService
{
    /**
     * @param $collections
     * @param $options
     * @return array
     */
    public function filter($collections, $options)
    {
        $collections->map(function ($collection) {
            $collection->priceUAH = $collection->currency->rate * $collection->price;
            return $collection;
        });
        if ($brand = Arr::pull($options, 'brand')) {
            $collections = $collections->where('id_brand', $brand);
        }
        if ($availability = Arr::pull($options, 'availability')) {
            $collections = $collections->where('id_availability', $availability);
        }
        if ($price = Arr::pull($options, 'price')) {
            $collections = $collections->whereBetween('priceUAH', [$price['from'], $price['to']]);
        }

        if ($sorting = Arr::pull($options, 'sorting')) {
            if($sorting['sortBy'] === 'name') {
                $column = $sorting['sortBy'];
            } elseif ($sorting['sortBy'] === 'price') {
                $column = $sorting['sortBy'];
            }
            if (isset($column))
            $collections = $sorting['isDesc'] ? $collections->sortByDesc($column) : $collections->sortBy($column);
        }
        return array_values($collections->toArray());
    }
}
